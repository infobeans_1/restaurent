# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

TableNo.delete_all
TableNo.create(name: 1)
TableNo.create(name: 2)
TableNo.create(name: 3)
TableNo.create(name: 4)
TableNo.create(name: 5)

Category.delete_all
Category.create([
    {name: "foods"},
    {name: "drinks"}
])

Menu.delete_all
Menu.create(item_name: 'sandwiches', quantity: '1', price: '3.00', category_id: Category.first.id)
Menu.create(item_name: 'borsch', quantity: '1', price: '2.00', category_id: Category.first.id)
Menu.create(item_name: 'margarita pizza', quantity: 'small', price: '3.00', category_id: Category.first.id)
Menu.create(item_name: 'latte', quantity: '1 cup', price: '3.00', category_id: Category.last.id)
Menu.create(item_name: 'green tea', quantity: '1 cup', price: '1.50', category_id: Category.last.id)
Menu.create(item_name: 'black tea', quantity: '1 cup', price: '1.00', category_id: Category.last.id)

Waiter.delete_all
Waiter.create([
    {name: "Sam"},
    {name: "David"}
])

Customer.delete_all
Customer.create([
    {name: "Trump", mob_no: 98082, table_no_id: 2}
])

#Order custmer_id:integer is_paid:boolean, payment_method:string
# Order.delete_all
Order.create(customer_id: Customer.first.id, is_paid: true, payment_method: 'cash', table_no_id: 2, waiter_id: 1)

# #rails g model CustmerOrder order_id:integer menu_id:integer quantity:integer total:float
# CustomerOrder.delete_all
# item1 = Menu.first
# CustomerOrder.create(order_id: Order.first.id, menu_id: item1.id, quantity: 2, total:item1.price*2)
# item2 = Menu.last
# CustomerOrder.create(order_id: Order.first.id, menu_id: item2.id, quantity: 2, total:item2.price*2)

# #rails g model TableOrders custmer_order_id:integer table_no_id:integer waiter_id:integer
# TableOrder.delete_all
# TableOrder.create(order_id: Order.last.id, table_no_id: TableNo.first.id, waiter_id: Waiter.first.id)


# User.delete_all
# User.create(name: 'Sam', email: 'waiter@infobeans.com', username:'waiter', password: 'waiter', password_confirmation: 'waiter', admin: false)
# User.create(name: 'Admin', email: 'admin@infobeans.com', username:'admin', password: 'admin', password_confirmation: 'admin', admin:true)
