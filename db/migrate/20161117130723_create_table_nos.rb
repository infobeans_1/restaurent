class CreateTableNos < ActiveRecord::Migration
  def change
    create_table :table_nos do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
