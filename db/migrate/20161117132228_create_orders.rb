class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :customer_id
      t.boolean :is_paid
      t.string :payment_method
      t.integer :table_no_id
      t.integer :waiter_id
      t.integer :customer_id

      t.timestamps null: false
    end
  end
end
