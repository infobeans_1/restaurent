class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :item_name
      t.string :quantity
      t.float :price
      t.integer :category_id

      t.timestamps null: false
    end
  end
end
