class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.integer :mob_no
      t.integer :table_no_id

      t.timestamps null: false
    end
  end
end
