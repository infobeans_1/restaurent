class Menu < ActiveRecord::Base
  has_many :menu_orders
  has_many :orders, :through => :menu_orders
  belongs_to :category
end
