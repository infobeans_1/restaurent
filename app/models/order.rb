class Order < ActiveRecord::Base
	belongs_to :customer
	belongs_to :waiter
	belongs_to :table_no
	has_many :menu_orders, :dependent => :destroy
	has_many :menus, :through => :menu_orders
end
