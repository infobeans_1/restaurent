class TableNo < ActiveRecord::Base
	has_many :customers, :dependent => :destroy
	has_many :orders, :dependent => :destroy
end
