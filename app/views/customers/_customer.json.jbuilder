json.extract! customer, :id, :name, :mob_no, :table_no_id, :created_at, :updated_at
json.url customer_url(customer, format: :json)