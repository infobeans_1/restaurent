json.extract! waiter, :id, :name, :dob, :created_at, :updated_at
json.url waiter_url(waiter, format: :json)