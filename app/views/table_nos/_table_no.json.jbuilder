json.extract! table_no, :id, :name, :created_at, :updated_at
json.url table_no_url(table_no, format: :json)