json.extract! menu, :id, :item_name, :quantity, :price, :category_id, :created_at, :updated_at
json.url menu_url(menu, format: :json)