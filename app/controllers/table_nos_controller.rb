class TableNosController < ApplicationController
  before_action :set_table_no, only: [:show, :edit, :update, :destroy]

  # GET /table_nos
  # GET /table_nos.json
  def index
    @table_nos = TableNo.all
  end

  # GET /table_nos/1
  # GET /table_nos/1.json
  def show
  end

  # GET /table_nos/new
  def new
    @table_no = TableNo.new
  end

  # GET /table_nos/1/edit
  def edit
  end

  # POST /table_nos
  # POST /table_nos.json
  def create
    @table_no = TableNo.new(table_no_params)

    respond_to do |format|
      if @table_no.save
        format.html { redirect_to @table_no, notice: 'Table no was successfully created.' }
        format.json { render :show, status: :created, location: @table_no }
      else
        format.html { render :new }
        format.json { render json: @table_no.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /table_nos/1
  # PATCH/PUT /table_nos/1.json
  def update
    respond_to do |format|
      if @table_no.update(table_no_params)
        format.html { redirect_to @table_no, notice: 'Table no was successfully updated.' }
        format.json { render :show, status: :ok, location: @table_no }
      else
        format.html { render :edit }
        format.json { render json: @table_no.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /table_nos/1
  # DELETE /table_nos/1.json
  def destroy
    @table_no.destroy
    respond_to do |format|
      format.html { redirect_to table_nos_url, notice: 'Table no was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_table_no
      @table_no = TableNo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def table_no_params
      params.require(:table_no).permit(:name)
    end
end
