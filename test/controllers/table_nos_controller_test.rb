require 'test_helper'

class TableNosControllerTest < ActionController::TestCase
  setup do
    @table_no = table_nos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:table_nos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create table_no" do
    assert_difference('TableNo.count') do
      post :create, table_no: { name: @table_no.name }
    end

    assert_redirected_to table_no_path(assigns(:table_no))
  end

  test "should show table_no" do
    get :show, id: @table_no
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @table_no
    assert_response :success
  end

  test "should update table_no" do
    patch :update, id: @table_no, table_no: { name: @table_no.name }
    assert_redirected_to table_no_path(assigns(:table_no))
  end

  test "should destroy table_no" do
    assert_difference('TableNo.count', -1) do
      delete :destroy, id: @table_no
    end

    assert_redirected_to table_nos_path
  end
end
